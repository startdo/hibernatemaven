package io.saagie.example.hive;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

/**
 * Hello world!
 *
 */
public class App {
	private static String driverName = "org.apache.hive.jdbc.HiveDriver";

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// try {

		Class.forName(driverName);
		Connection connection = null;
		System.out.println("Before getting connection");
		connection = DriverManager.getConnection("jdbc:hive2://localhost:10000/", "root", "hadoop");
		// create statement
		Statement stmt = connection.createStatement();

		System.out.println("After getting connection " + connection);

		// Create a database taxi
		stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS NyTaxi");
		// Create a new connection to the taxi DATABASE
		Connection connection1 = DriverManager.getConnection("jdbc:hive2://localhost:10000/NyTaxi", "root", "hadoop");
		// Create a table called TaxiDriver
		Statement stmt1 = connection1.createStatement();
		stmt1.execute("CREATE TABLE IF NOT EXISTS "
				+ " nytaxi (dropoff_datetime Timestamp, dropoff_latitude DECIMAL, dropoff_longitude DECIMAL, dropoff_taxizone_id int, ehail_fee DECIMAL,extra DECIMAL,fare_amount DECIMAL,improvement_surcharge DECIMAL,mta_tax DECIMAL, passenger_count int, payment_type String , pickup_datetime Timestamp, pickup_latitude DECIMAL, pickup_longitude DECIMAL,pickup_taxizone_id int, rate_code_id int, store_and_fwd_flag int,tip_amount DECIMAL, tolls_amount DECIMAL,total_amount DECIMAL, trip_distance DECIMAL, trip_type String,vendor_id String, trip_id INT, "
				+ " salary String, destignation String \n)" + " COMMENT 'Employee details' " + " ROW FORMAT DELIMITED "
				+ " FIELDS TERMINATED BY '\t'" + " LINES TERMINATED BY '\n'" + " STORED AS TEXTFILE");
		System.out.println("Table nytaxi created.");

		ResultSet resultSet = connection.createStatement().executeQuery("select dropoff_datetime from NyTaxi.nytaxi");

		while (resultSet.next()) {
			System.out.println(resultSet.getString(1) + " " + resultSet.getString(2));
		}

	}
}

// import java.io.IOException;
// import java.sql.Connection;
// import java.sql.DriverManager;
// import java.sql.ResultSet;
// import java.sql.Statement;
// import java.util.logging.Logger;
// public class Main {
//
//
// private static final Logger logger =
// Logger.getLogger("io.saagie.example.hive.Main");
// private static final String JDBC_DRIVER_NAME =
// "org.apache.hive.jdbc.HiveDriver";
// private static String connectionUrl;
//
// public static void main(String[] args) throws IOException {
//
// if (args.length < 1) {
// logger.severe("1 arg is required :\n\t- connectionurl ex:
// jdbc:hive2://hiveserver:10000/;ssl=false");
// System.err.println("1 arg is required :\n\t-connectionurl ex:
// jdbc:hive2://hiveserver:10000/;ssl=false");
// System.exit(128);
// }
// // Get url Connection
// connectionUrl = "jdbc:mysql://sandbox-hdp.hortonworks.com/hive";//args[0];
//
// // Init Connection
// Connection con = null;
//
// //==== Select data from Hive Table
// String sqlStatementDrop = "DROP TABLE IF EXISTS helloworld";
// String sqlStatementCreate = "CREATE TABLE helloworld (message String) STORED
// AS PARQUET";
// String sqlStatementInsert = "INSERT INTO helloworld VALUES (\"helloworld\")";
// String sqlStatementSelect = "SELECT * from helloworld";
// try {
// // Set JDBC Hive Driver
// Class.forName(JDBC_DRIVER_NAME);
// // Connect to Hive
// con = DriverManager.getConnection(connectionUrl,"hdfs","");
// // Init Statement
// Statement stmt = con.createStatement();
// // Execute DROP TABLE Query
// stmt.execute(sqlStatementDrop);
// logger.info("Drop Hive table : OK");
// // Execute CREATE Query
// stmt.execute(sqlStatementCreate);
// logger.info("Create Hive table : OK");
// // Execute INSERT Query
// stmt.execute(sqlStatementInsert);
// logger.info("Insert into Hive table : OK");
// // Execute SELECT Query
// ResultSet rs = stmt.executeQuery(sqlStatementSelect);
// while(rs.next()) {
// logger.info(rs.getString(1));
// }
// logger.info("Select from Hive table : OK");
//
// } catch (Exception e) {
// logger.severe(e.getMessage());
// } finally {
// try {
// con.close();
// } catch (Exception e) {
// // swallow
// }
// }
//
// }
// }
