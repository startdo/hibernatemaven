
package com.tcb.hiveInternalTables;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 */
public class HiveJdbcClientExample {
	private static String driverName = "com.mysql.jdbc.Driver";

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// try {

		Class.forName(driverName);
		Connection connection = null;
		System.out.println("Before getting connection");
		connection = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/", "root", "root");
		// create statement
		Statement stmt = connection.createStatement();

		System.out.println("After getting connection " + connection);

		// Create a database taxi
		stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS nytaxi");
		// Create a new connection to the taxi DATABASE
		Connection connection1 = DriverManager.getConnection("jdbc:mysql://172.17.0.2:3306/taxi", "root", "root");
		// Create a table called TaxiDriver
		Statement stmt1 = connection1.createStatement();
		stmt1.execute("CREATE TABLE IF NOT EXISTS " + " taxiDriver (eid int, name String, "
				+ " salary String, destignation String)" + " COMMENT 'Employee details' " + " ROW FORMAT DELIMITED "
				+ " FIELDS TERMINATED BY '\t'" + " LINES TERMINATED BY '\n'" + " STORED AS TEXTFILE");
		System.out.println("Table employee created.");

		ResultSet resultSet = connection.createStatement().executeQuery("select eid from default.employee");

		while (resultSet.next()) {
			System.out.println(resultSet.getString(1) + " " + resultSet.getString(2));
		}

	}
}