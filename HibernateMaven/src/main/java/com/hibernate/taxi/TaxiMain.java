package com.hibernate.taxi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.hadoop.fs.Path;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.engine.query.spi.sql.NativeSQLQueryReturn;
//import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.hadoop.example.GroupReadSupport;
import org.apache.parquet.schema.GroupType;
//import org.apache.hadoop.conf.Configuration;

public class TaxiMain {

	static TaxiTrip trip;
	static Session sessionObj;
	static SessionFactory sessionFactoryObj;

	private static SessionFactory buildSessionFactory() {
		// Creating Configuration Instance & Passing Hibernate Configuration
		// File
		org.hibernate.cfg.Configuration configObj = new org.hibernate.cfg.Configuration();
		configObj.configure("hibernate.cfg.xml");

		// Since Hibernate Version 4.x, ServiceRegistry Is Being Used
		ServiceRegistry serviceRegistryObj = new StandardServiceRegistryBuilder()
				.applySettings(configObj.getProperties()).build();

		// Creating Hibernate SessionFactory Instance
		sessionFactoryObj = configObj.buildSessionFactory(serviceRegistryObj);
		return sessionFactoryObj;
	}

	public static void main(String[] args) {
		System.out.println(".......Hibernate Maven Example.......\n");
		TimeConverter timeConverter = new TimeConverter();
		try {
			Path file = new Path("/home/omar/workspace/HibernateMaven/input/partest.snappy.parquet"); 
			//Path file = new Path("hdfs://192.168.1.111/user/admin/NYC_taxi_2009_2016/part-r-00400-ec9cbb65-519d-4bdb-a918-72e2364c144c.snappy.parquet"); 
			org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
			ParquetReader<Group> reader = ParquetReader.builder(new GroupReadSupport(), file).withConf(configuration).build();
			sessionObj = buildSessionFactory().openSession();
			sessionObj.beginTransaction();
			Group group = null;
			for (int i=0;i<5;i++)
			{
			group = reader.read();
			trip = new TaxiTrip();
			try{
			trip.setDropoff_datetime(timeConverter.getTimestamp(timeConverter.getDateTimeValueFromBinary(group.getInt96("dropoff_datetime", 0), true)));
			}catch (Exception e) {};
			try{
			trip.setDropoff_latitude(group.getFloat("dropoff_latitude", 0));
			}catch (Exception e) {};
			try{
			trip.setDropoff_longitude(group.getFloat("dropoff_longitude", 0));
			}catch (Exception e) {};
			try{
			trip.setDropoff_taxizone_id(group.getInteger("dropoff_taxizone_id", 0));
			}catch (Exception e) {};
			try{
			trip.setEhail_fee(group.getFloat("ehail_fee", 0));
			}catch (Exception e) {};
			try{
			trip.setExtra(group.getFloat("extra", 0));
			}catch (Exception e) {};
			try{
			trip.setFare_amount(group.getFloat("fare_amount", 0));
			}catch (Exception e) {};
			try{
			trip.setImprovement_surcharge(group.getFloat("improvement_surcharge", 0));
			}catch (Exception e) {};
			try{
			trip.setMta_tax(group.getFloat("mta_tax", 0));
			}catch (Exception e) {};
			try{
			trip.setPassenger_count(group.getInteger("passenger_count", 0));
			}catch (Exception e) {};
			try{
			trip.setPayment_type(group.getString("payment_type", 0));
			}catch (Exception e) {};
			try{
			trip.setPickup_datetime(timeConverter.getTimestamp(timeConverter.getDateTimeValueFromBinary(group.getInt96("pickup_datetime", 0), true)));
			}catch (Exception e) {};
			try{
			trip.setPickup_latitude(group.getFloat("pickup_latitude", 0));
			}catch (Exception e) {};
			try{
			trip.setPickup_longitude(group.getFloat("pickup_longitude", 0));
			}catch (Exception e) {};
			try{
			trip.setPickup_taxizone_id(group.getInteger("pickup_taxizone_id", 0));
			}catch (Exception e) {};
			try{
			trip.setRate_code_id(group.getInteger("rate_code_id", 0));
			}catch (Exception e) {};
			try{
			trip.setStore_and_fwd_flag(group.getString("store_and_fwd_flag", 0));
			}catch (Exception e) {};
			try{
			trip.setTip_amount(group.getFloat("tip_amount", 0));
			}catch (Exception e) {};
			try{
			trip.setTolls_amount(group.getFloat("tolls_amount", 0));
			}catch (Exception e) {};
			try{
			trip.setTotal_amount(group.getFloat("total_amount", 0));
			}catch (Exception e) {};
			try{
			trip.setTrip_distance(group.getFloat("trip_distance", 0));
			}catch (Exception e) {};
			try{
			trip.setTrip_type(group.getString("trip_type", 0));
			}catch (Exception e) {};
			try{
			trip.setVendor_id(group.getString("vendor_id", 0));
			}catch (Exception e) {};
			try{
			trip.setTrip_id(group.getLong("trip_id", 0));
			}catch (Exception e) {};
			sessionObj.save(trip);
			}		
			System.out.println("\n.......Records Saved Successfully To The Database.......\n");

			// Committing The Transactions To The Database
			sessionObj.getTransaction().commit();
			System.out.println("\n......Transactions Committed Successfully to The Database.........\n");
		} catch (Exception sqlException) {
			sqlException.printStackTrace();
			if (null != sessionObj.getTransaction()) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				sessionObj.getTransaction().rollback();
			}
			sqlException.printStackTrace();
		} finally {
			if (sessionObj != null) {
				sessionObj.close();
			}
		}
	}
}